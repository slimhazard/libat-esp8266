/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef RP2040_ESP_AT_H
#define RP2040_ESP_AT_H

#include <hardware/uart.h>

#define ESP_AT_SSID_MAXLEN (32)
#define ESP_AT_MACADDR_MAXLEN (17)
#define ESP_AT_IPV4_MAXLEN (16)

struct esp_at_hndl;

struct esp_at_hndl * const esp_at_hndl0;
struct esp_at_hndl * const esp_at_hndl1;

typedef enum {
              ESP_AT_ERR_OK = 0,
              ESP_AT_ERR_ERROR,
              ESP_AT_ERR_TIMEOUT,
              ESP_AT_ERR_BUF_OVERFLOW,
              ESP_AT_ERR_INVALID_RESPONSE,
              ESP_AT_ERR_NOMEM,
              ESP_AT_ERR_CX_TIMEOUT,
              ESP_AT_ERR_CX_FAIL,
              ESP_AT_ERR_AP_NOTFOUND,
              ESP_AT_ERR_CREDS,
              ESP_AT_ERR_ALREADY_CX,
              ESP_AT_ERR_DNS,
              ESP_AT_ERR_RX_BUF_OVERFLOW,
              ESP_AT_ERR_UNKNOWN,
              __ESP_AT_ERR_MAX,
} esp_at_err_t;

const char *esp_at_errstr[__ESP_AT_ERR_MAX];

esp_at_err_t esp_at_hndl_init(struct esp_at_hndl *hndl);
void esp_at_hndl_fini(struct esp_at_hndl *hndl);

esp_at_err_t esp_at_base_test_timeout_ms(struct esp_at_hndl *hndl, uint32_t ms);

esp_at_err_t esp_at_base_reset_timeout_ms(struct esp_at_hndl *hndl,
					  uint8_t *reset_buf, size_t buflen,
					  uint32_t ms);

esp_at_err_t esp_at_base_set_echo_timeout_ms(struct esp_at_hndl *hndl,
					     int enable, uint32_t ms);

esp_at_err_t esp_at_base_get_sysstore_timeout_ms(struct esp_at_hndl *hndl,
						 int *sysstore, uint32_t ms);

esp_at_err_t esp_at_base_set_sysstore_timeout_ms(struct esp_at_hndl *hndl,
						 int sysstore, uint32_t ms);

esp_at_err_t esp_at_base_deep_sleep_timeout_ms(struct esp_at_hndl *hndl,
					       uint32_t sleep_ms,
					       uint32_t timeout_ms);

typedef enum {
              ESP_AT_WIFI_MODE_STATION = 1,
              ESP_AT_WIFI_MODE_AP,
              ESP_AT_WIFI_MODE_STATION_AP,
              __ESP_AT_WIFI_MODE_MAX,
} esp_at_wifi_mode_t;

esp_at_err_t esp_at_wifi_get_mode_timeout_ms(struct esp_at_hndl *hndl,
					     esp_at_wifi_mode_t *mode,
					     uint32_t ms);
esp_at_err_t esp_at_wifi_set_mode_timeout_ms(struct esp_at_hndl *hndl,
					     esp_at_wifi_mode_t mode,
					     uint32_t ms);

struct esp_at_dhcp {
        unsigned int softap:1;
        unsigned int station:1;
        unsigned int enable:1;
};

esp_at_err_t esp_at_wifi_get_dhcp_timeout_ms(struct esp_at_hndl *hndl,
					     struct esp_at_dhcp *dhcp,
					     uint32_t ms);
esp_at_err_t esp_at_wifi_set_dhcp_timeout_ms(struct esp_at_hndl *hndl,
					     struct esp_at_dhcp *dhcp,
					     uint32_t ms);

struct esp_at_ap {
        char ssid[ESP_AT_SSID_MAXLEN + 1];
        char bssid[ESP_AT_MACADDR_MAXLEN + 1];
        int channel;
        int rssi;
        int pci_en;
	int reconn;
	int listen_interval;
	int scan_mode;
};

esp_at_err_t esp_at_wifi_get_ap_timeout_ms(struct esp_at_hndl *hndl,
					   struct esp_at_ap *ap, uint32_t ms);
esp_at_err_t esp_at_wifi_set_ap_timeout_ms(struct esp_at_hndl *hndl,
					   struct esp_at_ap *ap,
					   char *password, int *cx, int *ip,
					   uint32_t ms);
esp_at_err_t esp_at_wifi_disconnect_ap_timeout_ms(struct esp_at_hndl *hndl,
						  uint32_t ms);

typedef enum {
              ESP_AT_STATUS_UNINIT = 0,
              ESP_AT_STATUS_INIT,
              ESP_AT_STATUS_AP,
              ESP_AT_STATUS_CX,
              ESP_AT_STATUS_NO_CX,
              ESP_AT_STATUS_NO_AP,
              __ESP_AT_STATUS_MAX,
} esp_at_status_t;

typedef enum {
              ESP_AT_CX_TYPE_TCP = 0,
              ESP_AT_CX_TYPE_UDP,
              ESP_AT_CX_TYPE_SSL,
              __ESP_AT_CX_TYPE_MAX,
} esp_at_cx_type_t;

typedef enum {
              ESP_AT_ROLE_CLIENT = 0,
              ESP_AT_ROLE_SERVER,
              __ESP_AT_ROLE_MAX,
} esp_at_role_t;

struct esp_at_status {
	char *remote_ip;
	uint16_t remote_port;
	uint16_t local_port;
	esp_at_status_t status;
	esp_at_cx_type_t type;
	esp_at_role_t role;
	int8_t linkId;
};

struct esp_at_cx {
        char *addr;
        esp_at_cx_type_t type;
        uint16_t port;
        uint16_t keepalive;
        int8_t linkId;
};

esp_at_err_t esp_at_tcp_get_status_timeout_ms(struct esp_at_hndl *hndl,
					      struct esp_at_status *status,
					      uint32_t ms);

esp_at_err_t esp_at_tcp_connect_timeout_ms(struct esp_at_hndl *hndl,
					   struct esp_at_cx *cx, uint32_t ms);
esp_at_err_t esp_at_tcp_close_timeout_ms(struct esp_at_hndl *hndl,
					 int8_t linkId, uint32_t ms);

typedef enum {
              ESP_AT_RECV_MODE_ACTIVE = 0,
              ESP_AT_RECV_MODE_PASSIVE,
              __ESP_AT_RECV_MODE_MAX,
} esp_at_recv_mode_t;

esp_at_err_t esp_at_tcp_get_recvmode_timeout_ms(struct esp_at_hndl *hndl,
						esp_at_recv_mode_t *mode,
						uint32_t ms);
esp_at_err_t esp_at_tcp_set_recvmode_timeout_ms(struct esp_at_hndl *hndl,
						esp_at_recv_mode_t mode,
						uint32_t ms);

/* XXX: currently working only with passive receive mode */
esp_at_err_t esp_at_tcp_send_timeout_ms(struct esp_at_hndl *hndl, int8_t linkId,
					uint8_t *buf, size_t len, uint32_t ms);
esp_at_err_t esp_at_tcp_recv_timeout_ms(struct esp_at_hndl *hndl, int8_t linkId,
					uint8_t *buf, int len, int *actual_len,
					uint32_t ms);

#endif // RP2040_ESP_AT_H
