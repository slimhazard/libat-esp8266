
if (NOT OPENOCD_PATH)
  if (DEFINED ENV{OPENOCD_PATH})
    set(OPENOCD_PATH $ENV{OPENOCD_PATH})
    message("Using OPENOCD_PATH from environment ('${OPENOCD_PATH}')")
  else ()
    find_program (OPENOCD_PATH
      NAMES openocd
      DOC "path to openocd binary"
      )
  endif ()
endif ()

if (OPENOCD_PATH)
  message(STATUS "openocd: ${OPENOCD_PATH}")
  if (DEFINED ENV{OPENOCD_TCL_PATH} AND (NOT OPENOCD_TCL_PATH))
    set(OPENOCD_TCL_PATH $ENV{OPENOCD_TCL_PATH})
    message("Using OPENOCD_TCL_PATH from environment ('${OPENOCD_TCL_PATH}')")
  endif ()
endif ()

if (OPENOCD_TCL_PATH)
  message(STATUS "openocd tcl path: ${OPENOCD_TCL_PATH}")
endif ()

if (OPENOCD_PATH AND OPENOCD_TCL_PATH)
  message(STATUS "generating flash target")
  add_custom_target(flash
    COMMAND ${OPENOCD_PATH} -f ${OPENOCD_TCL_PATH}/interface/raspberrypi-swd.cfg -f ${OPENOCD_TCL_PATH}/target/rp2040.cfg -c \"program test_rp2040-esp-at.elf verify reset exit\"
    DEPENDS ELF2UF2Build
    COMMENT "Flashing with openocd"
  )
else ()
  message(WARNING "not generating flash target: OPENOCD_PATH and/or OPENOCD_TCL_PATH not set")
endif ()
