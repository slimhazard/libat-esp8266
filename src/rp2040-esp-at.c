/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/* for memset(3), memcpy(3), strchr(3) */
#include <string.h>

/* for sscanf(3) */
#include <stdio.h>

/* for isdigit(3) */
#include <ctype.h>

/* for malloc(3) & free(3): XXX: pico_malloc? "multicore safety" */
#include <stdlib.h>

#include <hardware/structs/uart.h>
#include <pico/time.h>

#include "rp2040-esp-at.h"

#define RESPONSE_BUFLEN (128)

#define cmp(buf, s) (strncmp(buf, s, sizeof(s)) == 0)

struct esp_at_hndl {
        uart_inst_t	*uart;
        uint8_t		*buf;
        unsigned	buflen;
};

static struct esp_at_hndl esp_at_hndl0_obj, esp_at_hndl1_obj;

struct esp_at_hndl * const esp_at_hndl0 = &esp_at_hndl0_obj,
        * const esp_at_hndl1 = &esp_at_hndl1_obj;

const char *esp_at_errstr[] =
        {
         [ESP_AT_ERR_OK]		= "OK",
         [ESP_AT_ERR_ERROR]		= "ERROR",
         [ESP_AT_ERR_TIMEOUT]		= "timeout",
         [ESP_AT_ERR_BUF_OVERFLOW]	= "internal buffer overflow",
         [ESP_AT_ERR_INVALID_RESPONSE]	= "invalid response",
         [ESP_AT_ERR_NOMEM]		= "out of memory",
         [ESP_AT_ERR_CX_TIMEOUT]	= "connection timeout",
         [ESP_AT_ERR_CX_FAIL]		= "connection failed",
         [ESP_AT_ERR_AP_NOTFOUND]	= "target AP not found",
         [ESP_AT_ERR_CREDS]		= "wrong password",
         [ESP_AT_ERR_ALREADY_CX]	= "already connected",
         [ESP_AT_ERR_DNS]		= "DNS failure",
         [ESP_AT_ERR_RX_BUF_OVERFLOW]	= "recv buffer overflow",
         [ESP_AT_ERR_UNKNOWN]		= "unknown error",
        };

static inline int
send_cmd(uart_inst_t *uart, const uint8_t *cmd, absolute_time_t deadline)
{
        while (!uart_is_writable(uart))
                if (time_reached(deadline))
                        return (-1);
        uart_puts(uart, cmd);
        uart_putc_raw(uart, '\r');
        uart_putc_raw(uart, '\n');
        return (0);
}

static inline int
parse_string(char *dst, uint8_t **src, uint8_t *limit)
{
        uint8_t *start, *end;

        if ((start = memchr(*src, '\"', limit - *src)) == NULL)
                return (-1);
        if ((end = memchr(start + 1, '\"', limit - start - 1)) == NULL)
                return (-1);
        memcpy(dst, start + 1, end - start - 1);
        dst[end - start - 1] = '\0';
        *src = end + 1;
        return (0);
}

static inline int
parse_decimal(int *dst, uint8_t **src, uint8_t delim)
{
        int pos = 1;
        uint8_t *d = *src;

        *dst = 0;
        if (*d == '-') {
                pos = 0;
                d++;
        }
        for ( ; *d != '\r' && *d != delim; d++) {
                if (!isdigit(*d))
                        return (-1);
                *dst *= 10;
                *dst += *d - '0';
        }
        if (!pos)
                *dst = -(*dst);
        *src = d;
        return (0);
}

esp_at_err_t
esp_at_hndl_init(struct esp_at_hndl *hndl)
{
        hndl->buf = malloc(RESPONSE_BUFLEN);
        if (hndl->buf == NULL)
                return (ESP_AT_ERR_NOMEM);
        hndl->buflen = RESPONSE_BUFLEN;
        if (hndl == esp_at_hndl0)
                hndl->uart = uart0;
        else
                hndl->uart = uart1;
        return (ESP_AT_ERR_OK);
}

void
esp_at_hndl_fini(struct esp_at_hndl *hndl)
{
	free(hndl->buf);
}

static const uint8_t cmd_base_test[] = "AT";

esp_at_err_t
esp_at_base_test_timeout_ms(struct esp_at_hndl *hndl, uint32_t ms)
{
        unsigned i;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_base_test, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t cmd_base_reset[] = "AT+RST";

esp_at_err_t
esp_at_base_reset_timeout_ms(struct esp_at_hndl *hndl, uint8_t *reset_buf,
    size_t buflen, uint32_t ms)
{
        unsigned i;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_base_reset, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;

        /* Drain the output after reset, saving it if requested. */
        for (i = 0; uart_is_readable(hndl->uart); i++)
                if (reset_buf != NULL && i < buflen)
                        reset_buf[i] = uart_getc(hndl->uart);
        return (ret);
}

esp_at_err_t
esp_at_base_set_echo_timeout_ms(struct esp_at_hndl *hndl, int enable,
    uint32_t ms)
{
        uint8_t cmd[] = "ATE ";
        unsigned i;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        cmd[3] = '0' + (enable != 0);
        if (send_cmd(hndl->uart, cmd_base_test, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t cmd_base_get_sysstore[] = "AT+SYSSTORE?",
        format_base_get_sysstore[] = "SYSSTORE:%d";

esp_at_err_t
esp_at_base_get_sysstore_timeout_ms(struct esp_at_hndl *hndl, int *sysstore,
				    uint32_t ms)
{
        unsigned i;
        int halt = 0;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

	*sysstore = -1;
        if (send_cmd(hndl->uart, cmd_base_get_sysstore, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; !halt && i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case '+':
                                sscanf(hndl->buf+1,
				       format_base_get_sysstore, sysstore);
                                break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
                                        ret = ESP_AT_ERR_OK;
                                        halt = 1;
                                }
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
                                        ret = ESP_AT_ERR_ERROR;
                                        halt = 1;
                                }
                        }
                        i = -1;
                        if (!halt)
                                memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;
        if (*sysstore == -1)
                return (ESP_AT_ERR_INVALID_RESPONSE);

        return (ret);
}

esp_at_err_t
esp_at_base_set_sysstore_timeout_ms(struct esp_at_hndl *hndl, int sysstore,
				    uint32_t ms)
{
        unsigned i;
        uint8_t cmd[] = "AT+SYSSTORE= ";
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        cmd[sizeof("AT+SYSSTORE=") - 1] = '0' + (sysstore != 0);
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }

        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;
        return (ret);
}

static const uint8_t format_base_deep_sleep[] = "AT+GSLP=%u";

esp_at_err_t
esp_at_base_deep_sleep_timeout_ms(struct esp_at_hndl *hndl, uint32_t sleep_ms,
				  uint32_t timeout_ms)
{
        unsigned i;
        uint8_t cmd[sizeof("AT+GSLP=4294967295")];
        absolute_time_t deadline = make_timeout_time_ms(timeout_ms);

	sprintf(cmd, format_base_deep_sleep, sleep_ms);
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }

        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t cmd_wifi_get_mode[] = "AT+CWMODE?",
        format_wifi_get_mode[] = "CWMODE:%d";

esp_at_err_t
esp_at_wifi_get_mode_timeout_ms(struct esp_at_hndl *hndl,
				esp_at_wifi_mode_t *mode, uint32_t ms)
{
        unsigned i;
        int m = -1, parse_err = 0, halt = 0;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_wifi_get_mode, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; !halt && i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case '+':
                                if (sscanf(hndl->buf+1, format_wifi_get_mode,
					   &m) != 1)
                                        parse_err = 1;
                                break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
                                        ret = ESP_AT_ERR_OK;
                                        halt = 1;
                                }
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
                                        ret = ESP_AT_ERR_ERROR;
                                        halt = 1;
                                }
                        }
                        i = -1;
                        if (!halt)
                                memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;
        if (parse_err || m == -1)
                return (ESP_AT_ERR_INVALID_RESPONSE);
        *mode = m;

        return (ret);
}

esp_at_err_t
esp_at_wifi_set_mode_timeout_ms(struct esp_at_hndl *hndl,
				esp_at_wifi_mode_t mode, uint32_t ms)
{
        unsigned i;
        uint8_t cmd[] = "AT+CWMODE= ";
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        cmd[sizeof("AT+CWMODE=") - 1] = '0' + mode;
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }

        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;
        return (ret);
}

static const uint8_t cmd_wifi_get_dhcp[] = "AT+CWDHCP?",
	format_wifi_get_dhcp[] = "CWDHCP:%d";

esp_at_err_t
esp_at_wifi_get_dhcp_timeout_ms(struct esp_at_hndl *hndl,
				struct esp_at_dhcp *dhcp, uint32_t ms)
{
        unsigned i;
        int parse_err = 0, halt = 0, answer;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_wifi_get_dhcp, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);
        dhcp->enable = 0;

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; !halt && i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case '+':
                                if (sscanf(hndl->buf+1, format_wifi_get_dhcp,
					   &answer) != 1) {
                                        parse_err = 1;
                                        break;
                                }
                                dhcp->softap = ((answer & 1) != 0);
                                dhcp->station = ((answer & 2) != 0);
                                dhcp->enable = 1;
                                break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
                                        ret = ESP_AT_ERR_OK;
                                        halt = 1;
                                }
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
                                        ret = ESP_AT_ERR_ERROR;
                                        halt = 1;
                                }
                        }
                        i = -1;
                        if (!halt)
                                memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        if (parse_err || !dhcp->enable)
                return (ESP_AT_ERR_INVALID_RESPONSE);

        return (ret);
}

#define dhcp_set_mode_pos (sizeof("AT+CWDHCP=") - 1)
#define dhcp_set_en_pos (sizeof("AT+CWDHCP=X,") - 1)

esp_at_err_t
esp_at_wifi_set_dhcp_timeout_ms(struct esp_at_hndl *hndl,
				struct esp_at_dhcp *dhcp, uint32_t ms)
{
        unsigned i;
        uint8_t cmd[] = "AT+CWDHCP= , ";
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (dhcp->softap == dhcp->station)
                cmd[dhcp_set_mode_pos] = '2';
        else if (dhcp->softap)
                cmd[dhcp_set_mode_pos] = '0';
        else if (dhcp->station)
                cmd[dhcp_set_mode_pos] = '1';
        if (dhcp->enable)
                cmd[dhcp_set_en_pos] = '1';
        else
                cmd[dhcp_set_en_pos] = '0';
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }

        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t cmd_get_ap[] = "AT+CWJAP?", pfx_get_ap[] = "CWJAP:";
#define pfxlen_get_ap (sizeof(pfx_get_ap) - 1)

esp_at_err_t
esp_at_wifi_get_ap_timeout_ms(struct esp_at_hndl *hndl, struct esp_at_ap *ap,
			      uint32_t ms)
{
        uint8_t *p;
        unsigned i;
        int parse_err = 0, halt = 0;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_get_ap, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

	*ap->ssid = '\0';
	*ap->bssid = '\0';
        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; !halt && i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case '+':
                                if (strncmp(hndl->buf+1, pfx_get_ap,
					    pfxlen_get_ap)
				    != 0) {
                                        parse_err = 1;
                                        break;
                                }

                                p = hndl->buf + pfxlen_get_ap + 1;
                                if (parse_string(ap->ssid, &p,
						 hndl->buf + i - 1)
                                    != 0) {
                                        parse_err = 1;
                                        break;
                                }
                                p++;
                                if (parse_string(ap->bssid, &p,
						 hndl->buf + i - 1)
                                    != 0) {
                                        parse_err = 1;
                                        break;
                                }

                                p++;
                                if (parse_decimal(&ap->channel, &p, ',') != 0) {
                                        parse_err = 1;
                                        break;
                                }
                                p++;
                                if (parse_decimal(&ap->rssi, &p, ',') != 0) {
                                        parse_err = 1;
                                        break;
                                }
                                p++;
                                if (parse_decimal(&ap->pci_en, &p, ',') != 0) {
                                        parse_err = 1;
					break;
				}
                                p++;
                                if (parse_decimal(&ap->reconn, &p, ',') != 0) {
                                        parse_err = 1;
					break;
				}
                                p++;
                                if (parse_decimal(&ap->listen_interval, &p, ',')
				    != 0) {
                                        parse_err = 1;
					break;
				}
                                p++;
                                if (parse_decimal(&ap->scan_mode, &p, '\r')
				    != 0)
                                        parse_err = 1;
                                break;
                        case 'N':
                                if (cmp(hndl->buf+1, "o AP\r\n")) {
                                        *ap->ssid = '\0';
                                        *ap->bssid = '\0';
                                }
                                break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
                                        ret = ESP_AT_ERR_OK;
                                        halt = 1;
                                }
                                break;
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
                                        ret = ESP_AT_ERR_ERROR;
                                        halt = 1;
                                }
                                break;
                        }
                        i = -1;
                        if (!halt)
                                memset(hndl->buf, 0, hndl->buflen);
                }
        }

        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;
        if (parse_err)
                return (ESP_AT_ERR_INVALID_RESPONSE);
        return (ret);
}

static const uint8_t *format_wifi_set_ap[] =
        {
         "AT+CWJAP=\"%s\",\"%s\"",
         "AT+CWJAP=\"%s\",\"%s\",\"%s\"",
         "AT+CWJAP=\"%s\",\"%s\",\"%s\",%c",
        };

#define wifi_set_ap_baselen (sizeof("AT+CWJAP=\"\",\"\""))

/* XXX: escaping quotes & commas */
esp_at_err_t
esp_at_wifi_set_ap_timeout_ms(struct esp_at_hndl *hndl, struct esp_at_ap *ap,
			      char *password, int *cx, int *ip, uint32_t ms)
{
        uint8_t *cmd;
        size_t len = wifi_set_ap_baselen;
        unsigned i;
        int parse_err = 0, halt = 0, fail = 0, cx_err;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        *cx = *ip = 0;

        len += strlen(ap->ssid);
        len += strlen(password);
        if (ap->bssid != NULL)
                len += strlen(ap->bssid) + 3; // for quotation marks & comma
        if (ap->pci_en >= 0)
                len += 2;
        cmd = malloc(len);
        if (cmd == NULL)
                return (ESP_AT_ERR_NOMEM);

        if (ap->pci_en >= 0)
                snprintf(cmd, len, format_wifi_set_ap[2], ap->ssid, password,
			 ap->bssid == NULL ? "" : ap->bssid,
			 ap->pci_en ? '!' : '0');
        else if (ap->bssid[0] != '\0')
                snprintf(cmd, len, format_wifi_set_ap[1], ap->ssid, password,
			 ap->bssid);
        else
                snprintf(cmd, len, format_wifi_set_ap[0], ap->ssid, password);

        i = send_cmd(hndl->uart, cmd, deadline);
        free(cmd);
        if (i != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; !halt && i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'W':
                                if (cmp(hndl->buf+1, "IFI CONNECTED\r\n"))
                                        *cx = 1;
                                else if (cmp(hndl->buf+1, "IFI GOT IP\r\n"))
                                        *ip = 1;
                                else if (cmp(hndl->buf+1,
                                        "IFI DISCONNECT\r\n")) {
                                        *cx = 0;
                                        if (fail)
                                                halt = 1;
                                }
                                else
                                        parse_err = 1;
                                break;
                        case '+':
                                if (sscanf(hndl->buf+1, "CWJAP:%d\r\n",
                                        &cx_err) != 1) {
                                        parse_err = 1;
                                        break;
                                }
                                switch (cx_err) {
                                case 1:
                                        ret = ESP_AT_ERR_CX_TIMEOUT;
                                        break;
                                case 2:
                                        ret = ESP_AT_ERR_CREDS;
                                        break;
                                case 3:
                                        ret = ESP_AT_ERR_AP_NOTFOUND;
                                        break;
                                case 4:
                                        ret = ESP_AT_ERR_CX_FAIL;
                                        break;
                                default:
                                        ret = ESP_AT_ERR_UNKNOWN;
                                }
                                break;
                        case 'F':
                                if (cmp(hndl->buf+1, "AIL\r\n"))
                                        fail = 1;
                                break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
                                        ret = ESP_AT_ERR_OK;
                                        halt = 1;
                                }
                                break;
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
                                        ret = ESP_AT_ERR_ERROR;
                                        halt = 1;
                                }
                                break;
                        }
                        i = -1;
                        if (!halt)
                                memset(hndl->buf, 0, hndl->buflen);
                }
        }

        if (i == hndl->buflen)
                ret = ESP_AT_ERR_BUF_OVERFLOW;
        if (parse_err)
                return (ESP_AT_ERR_INVALID_RESPONSE);
        return (ret);
}

static const uint8_t cmd_wifi_disconnect[] = "AT+CWQAP";

esp_at_err_t
esp_at_wifi_disconnect_ap_timeout_ms(struct esp_at_hndl *hndl, uint32_t ms)
{
        unsigned i;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_wifi_disconnect, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t tcp_status_cmd[] = "AT+CIPSTATUS";

#define CIPSTATUS_PFX_LEN (sizeof("CIPSTATUS:")-1)
#define CIPSTATUS_TYPE_LEN (sizeof("\"XXX\"")-1)

/* XXX: probably working only in single connection mode, not mux mode */
esp_at_err_t
esp_at_tcp_get_status_timeout_ms(struct esp_at_hndl *hndl,
				 struct esp_at_status *status, uint32_t ms)
{
        esp_at_err_t ret = ESP_AT_ERR_UNKNOWN;
        unsigned i;
	int n, halt = 0;
	uint8_t *p;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, tcp_status_cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

	status->remote_ip[0] = '\0';
        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; !halt && i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'S':
                                sscanf(hndl->buf+1, "TATUS:%d\r\n",
				       &status->status);
				break;
                        case '+':
                                if (strncmp(hndl->buf+1, "CIPSTATUS:",
					    CIPSTATUS_PFX_LEN) != 0)
                                        break;
				p = hndl->buf + CIPSTATUS_PFX_LEN + 1;
				if (parse_decimal(&n, &p, ',') != 0) {
					ret = ESP_AT_ERR_INVALID_RESPONSE;
					break;
				}
				status->linkId = n;
				p++;
				if (strncmp(p, "\"TCP\"", CIPSTATUS_TYPE_LEN)
				    == 0)
					status->type = ESP_AT_CX_TYPE_TCP;
				else if (strncmp(p, "\"UDP\"",
						 CIPSTATUS_TYPE_LEN) == 0)
					status->type = ESP_AT_CX_TYPE_UDP;
				else {
					ret = ESP_AT_ERR_INVALID_RESPONSE;
					break;
				}
				p += CIPSTATUS_TYPE_LEN + 1;
				if (parse_string(status->remote_ip, &p,
						 hndl->buf + i - 1) != 0) {
					ret = ESP_AT_ERR_INVALID_RESPONSE;
					break;
				}
				p++;
				if (parse_decimal(&n, &p, ',') != 0) {
					ret = ESP_AT_ERR_INVALID_RESPONSE;
					break;
				}
				status->remote_port = (uint16_t)n;
				p++;
				if (parse_decimal(&n, &p, ',') != 0) {
					ret = ESP_AT_ERR_INVALID_RESPONSE;
					break;
				}
				status->local_port = (uint16_t)n;
				p++;
				if (parse_decimal(&n, &p, '\r') != 0) {
					ret = ESP_AT_ERR_INVALID_RESPONSE;
					break;
				}
				status->role = n;
				break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
					if (ret == ESP_AT_ERR_UNKNOWN)
						ret = ESP_AT_ERR_OK;
					halt = 1;
				}
				break;
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
					if (ret == ESP_AT_ERR_UNKNOWN)
						ret = ESP_AT_ERR_ERROR;
					halt = 1;
				}
                        }
                        i = -1;
			if (!halt)
				memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
	if (ret != ESP_AT_ERR_UNKNOWN)
		return (ret);
	return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t *format_tcp_connect[] =
        {
         "AT+CIPSTART=\"%s\",\"%s\",%u,%d",
         "AT+CIPSTART=%d,\"%s\",\"%s\",%u,%d",
        },
        *tcp_type_str[] = { "TCP", "UDP", "SSL" };

/*
 * strlen(AT+CIPSTART=XXX) + 45 (max ipv6 addr) + 5 (max port) + 1 (linkId)
 *	+ 4 (keepalive) + 5 commas & quotes + 1 terminating null
 */
#define TCP_CONNECT_MAXLEN (76)

esp_at_err_t
esp_at_tcp_connect_timeout_ms(struct esp_at_hndl *hndl, struct esp_at_cx *cx,
			      uint32_t ms)
{
        uint8_t cmd[TCP_CONNECT_MAXLEN];
        esp_at_err_t ret = ESP_AT_ERR_UNKNOWN;
        unsigned i;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (cx->linkId < 0 || cx->linkId > 4)
                snprintf(cmd, TCP_CONNECT_MAXLEN, format_tcp_connect[0],
                    tcp_type_str[cx->type], cx->addr, cx->port, cx->keepalive);
        else
                snprintf(cmd, TCP_CONNECT_MAXLEN, format_tcp_connect[1],
                    cx->linkId, tcp_type_str[cx->type], cx->addr, cx->port,
                    cx->keepalive);
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'A':
                                if (cmp(hndl->buf+1, "LREADY CONNECTED\r\n"))
                                        ret = ESP_AT_ERR_ALREADY_CX;
				break;
                        case 'D':
                                if (cmp(hndl->buf+1, "NS Fail\r\n"))
                                        ret = ESP_AT_ERR_DNS;
				break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n")) {
                                        if (ret != ESP_AT_ERR_UNKNOWN)
                                                return (ret);
                                        return (ESP_AT_ERR_ERROR);
                                }
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t cmd_tcp_single_close[] = "AT+CIPCLOSE";
static const uint8_t format_tcp_mux_close[] = "AT+CIPCLOSE=%d";

#define TCP_CLOSE_MUX_LEN (sizeof(format_tcp_mux_close)-2)

esp_at_err_t
esp_at_tcp_close_timeout_ms(struct esp_at_hndl *hndl, int8_t linkId,
			    uint32_t ms)
{
        uint8_t cmd_mux[TCP_CONNECT_MAXLEN];
        const uint8_t *cmd;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        unsigned i;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (linkId < 0 || linkId > 5)
                cmd = cmd_tcp_single_close;
        else {
                snprintf(cmd_mux, TCP_CLOSE_MUX_LEN, format_tcp_mux_close,
			 linkId);
                cmd = cmd_mux;
        }
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        /* XXX: check for CLOSED */
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
                                        return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

static const uint8_t cmd_get_recvmode[] = "AT+CIPRECVMODE?";

esp_at_err_t
esp_at_tcp_get_recvmode_timeout_ms(struct esp_at_hndl *hndl,
				   esp_at_recv_mode_t *mode, uint32_t ms)
{
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        unsigned i;
	int m = -1;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (send_cmd(hndl->uart, cmd_get_recvmode, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case '+':
                                if (sscanf(hndl->buf + 1, "CIPRECVMODE:%d\r\n",
					   &m) == 1)
					*mode = m;
				break;
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n")) {
					if (m == 0 || m == 1)
						return (ESP_AT_ERR_OK);
					return (ESP_AT_ERR_INVALID_RESPONSE);
				}
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

#define CMD_SET_RECVMODE ("AT+CIPRECVMODE= ")
#define CMD_SET_RECVMODE_LEN (sizeof(CMD_SET_RECVMODE)-1)

esp_at_err_t
esp_at_tcp_set_recvmode_timeout_ms(struct esp_at_hndl *hndl,
				   esp_at_recv_mode_t mode, uint32_t ms)
{
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
	uint8_t cmd_set_recvmode[] = CMD_SET_RECVMODE;
        unsigned i;
        absolute_time_t deadline = make_timeout_time_ms(ms);

	cmd_set_recvmode[CMD_SET_RECVMODE_LEN-1] = '0' + mode;
        if (send_cmd(hndl->uart, cmd_set_recvmode, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        switch (hndl->buf[0]) {
                        case 'O':
                                if (cmp(hndl->buf+1, "K\r\n"))
					return (ESP_AT_ERR_OK);
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
			}
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
		}
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        return (ESP_AT_ERR_TIMEOUT);
}

/* XXX: UDP */
static const uint8_t *format_tcp_send[] =
        {
         "AT+CIPSEND=%d",
         "AT+CIPSEND=%d,%d",
        };

/*
 * Max length is 2048 for ESP8266, 8192 for ESP32, so the longest
 * command is AT+CIPSEND=n,NNNN
 */
#define TCP_SEND_MAXLEN (18)

/* XXX: check len for max 2048/8192 */
esp_at_err_t
esp_at_tcp_send_timeout_ms(struct esp_at_hndl *hndl, int8_t linkId,
			    uint8_t *buf, size_t len, uint32_t ms)
{
        uint8_t cmd[TCP_SEND_MAXLEN];
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        unsigned i;
        int ok = 0, rxidx = 0;
        absolute_time_t deadline = make_timeout_time_ms(ms);

        if (linkId < 0 || linkId > 4)
                snprintf(cmd, TCP_SEND_MAXLEN, format_tcp_send[0], len);
        else
                snprintf(cmd, TCP_SEND_MAXLEN, format_tcp_send[1], linkId,
			 len);
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (i == 0 && hndl->buf[0] == '>')
                        break;
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        /* XXX: check for OK? */
                        switch (hndl->buf[0]) {
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        if (time_reached(deadline))
                return (ESP_AT_ERR_TIMEOUT);

        for (i = 0; i < len && !time_reached(deadline); i++) {
                if (!uart_is_writable(hndl->uart)) {
                        i--;
                        continue;
                }
                uart_putc_raw(hndl->uart, buf[i]);
        }
        if (time_reached(deadline))
                return (ESP_AT_ERR_TIMEOUT);

        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !ok && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
                hndl->buf[i] = uart_getc(hndl->uart);
                if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
                        /* XXX: check for 'Recv n bytes' and n==txlen? */
                        switch (hndl->buf[0]) {
                        case 'E':
                                if (cmp(hndl->buf+1, "RROR\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                        case 'S':
                                if (cmp(hndl->buf+1, "END OK\r\n"))
                                        ok = 1;
                                else if (cmp(hndl->buf+1, "END FAIL\r\n"))
                                        return (ESP_AT_ERR_ERROR);
                                break;
                        }
                        i = -1;
                        memset(hndl->buf, 0, hndl->buflen);
                }
        }
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
        if (time_reached(deadline))
                return (ESP_AT_ERR_TIMEOUT);
        return (ESP_AT_ERR_OK);
}

/* XXX: UDP */
static const uint8_t *format_tcp_recv[] =
        {
         "AT+CIPRECVDATA=%d",
         "AT+CIPRECVDATA=%d,%d",
        },
	pfx_tcp_recv[] = "+CIPRECVDATA:";

/*
 * Max length is 2^31 - 1 (ten decimal digits), so the longest command is
 * strlen("AT+CIPRECVDATA=n,") + 10.
 */
#define TCP_RECV_MAXLEN (27)

esp_at_err_t
esp_at_tcp_recv_timeout_ms(struct esp_at_hndl *hndl, int8_t linkId,
			   uint8_t *buf, int len, int *actual_len, uint32_t ms)
{
        uint8_t cmd[TCP_RECV_MAXLEN], *p = buf, *end;
        esp_at_err_t ret = ESP_AT_ERR_TIMEOUT;
        unsigned i;
        int state = 0;
        absolute_time_t deadline = make_timeout_time_ms(ms);

	memset(hndl->buf, 0, hndl->buflen);
	for (i = 0; i < hndl->buflen&& !time_reached(deadline); i++) {
		if (!uart_is_readable(hndl->uart)) {
			i--;
			continue;
		}
		hndl->buf[i] = uart_getc(hndl->uart);
		if (hndl->buf[i] == '\n' && i != 0 && hndl->buf[i-1] == '\r') {
			/*
			 * XXX parse the length, check overflow, only
			 * read the N bytes indicated by *IPD.
			 */
			if (strncmp(hndl->buf, "+IPD", 4) == 0)
				break;
			i = -1;
			memset(hndl->buf, 0, hndl->buflen);
		}
	}
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
	if (time_reached(deadline))
		return (ESP_AT_ERR_TIMEOUT);

        if (linkId < 0 || linkId > 4)
                snprintf(cmd, TCP_RECV_MAXLEN, format_tcp_recv[0], len);
        else
                snprintf(cmd, TCP_RECV_MAXLEN, format_tcp_recv[1], linkId, len);
        if (send_cmd(hndl->uart, cmd, deadline) != 0)
                return (ESP_AT_ERR_TIMEOUT);

	*actual_len = 0;
        memset(hndl->buf, 0, hndl->buflen);
        for (i = 0; i < hndl->buflen && !time_reached(deadline); i++) {
                if (!uart_is_readable(hndl->uart)) {
                        i--;
                        continue;
                }
		if (state == 2) {
			*p = uart_getc(hndl->uart);
			p++;
			if (p == end) {
				state = 0;
				memset(hndl->buf, 0, hndl->buflen);
			}
			i = -1;
			continue;
		}
                hndl->buf[i] = uart_getc(hndl->uart);
		if (state == 0) {
			if (hndl->buf[i] == ':'
			    && cmp(hndl->buf, pfx_tcp_recv)) {
				state = 1;
				continue;
			}
			if (hndl->buf[i] == '\n' && i != 0
			    && hndl->buf[i-1] == '\r') {
				switch (hndl->buf[0]) {
				case 'E':
					if (cmp(hndl->buf+1, "RROR\r\n"))
						return (ESP_AT_ERR_ERROR);
				case 'O':
					if (cmp(hndl->buf+1, "K\r\n"))
						return (ESP_AT_ERR_OK);
				}
				i = -1;
				memset(hndl->buf, 0, hndl->buflen);
			}
		}
		else if (hndl->buf[i] == ',') {
			if (strncmp(hndl->buf, pfx_tcp_recv,
				    sizeof(pfx_tcp_recv) - 1) != 0)
				continue;
			uint8_t *b = hndl->buf + sizeof(pfx_tcp_recv) - 1;
			if (parse_decimal(actual_len, &b, ',') != 0)
				return (ESP_AT_ERR_INVALID_RESPONSE);
			if (*actual_len > len)
				return (ESP_AT_ERR_RX_BUF_OVERFLOW);
			end = buf + *actual_len;
			state = 2;
			continue;
		}
	}
        if (i == hndl->buflen)
                return (ESP_AT_ERR_BUF_OVERFLOW);
	return (ESP_AT_ERR_TIMEOUT);
}
