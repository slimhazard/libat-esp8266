/*
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>

#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/uart.h"
#include "hardware/gpio.h"
#include "tusb.h"

#include <rp2040-esp-at.h>

#include "creds.h"

#define UART (PICO_DEFAULT_UART_INSTANCE)

#define TIMEOUT (secs2ms(10))
#define RESET_BUFLEN (128)

#define LINE_BUFLEN (128)

#define RX_BUFLEN (1024)

static inline uint32_t
secs2ms(int secs)
{
        return (secs * 1000);
}

int
main(void)
{
        esp_at_err_t err;
        int tests = 0, errs = 0;
        absolute_time_t from, to;

        uint8_t linebuf[LINE_BUFLEN];

        uint8_t reset_buf[RESET_BUFLEN];
        esp_at_wifi_mode_t mode, modes[2];
        struct esp_at_dhcp dhcp;
        struct esp_at_ap ap;
	struct esp_at_status status;
	uint8_t remote_ip[ESP_AT_IPV4_MAXLEN];
        char *password;
        int val, cx_status, ip, resp_len;

	esp_at_recv_mode_t recv_mode;

        stdio_usb_init();
        uart_init(UART, PICO_DEFAULT_UART_BAUD_RATE);

        gpio_set_function(PICO_DEFAULT_UART_TX_PIN, GPIO_FUNC_UART);
        gpio_set_function(PICO_DEFAULT_UART_RX_PIN, GPIO_FUNC_UART);
        gpio_pull_up(PICO_DEFAULT_UART_RX_PIN);

        /* Wait until someone is connected. */
        while (!tud_cdc_connected())
                sleep_ms(100);

        tests++;
        from = get_absolute_time();
        err = esp_at_hndl_init(esp_at_hndl0);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_hndl_init: OK");
        else {
                printf("esp_at_hndl_init: FAIL: %s\n", esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

#if 0
        tests++;
        from = get_absolute_time();
        err = esp_at_base_reset_timeout_ms(esp_at_hndl0, reset_buf,
					   RESET_BUFLEN, secs2ms(60));
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_base_reset_timeout_ms: OK");
        else {
                printf("esp_at_base_reset_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));
#endif

        tests++;
        from = get_absolute_time();
        err = esp_at_base_test_timeout_ms(esp_at_hndl0, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_base_test_timeout_ms: OK");
        else {
                printf("esp_at_base_test_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_base_reset_timeout_ms(esp_at_hndl0, NULL, 0, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_base_reset_timeout_ms: OK");
        else {
                printf("esp_at_base_reset_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        puts("Waiting 1s ...");
        sleep_ms(1000);
        puts("... done.");

        tests++;
        from = get_absolute_time();
        err = esp_at_base_set_echo_timeout_ms(esp_at_hndl0, 0, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_base_set_echo_timeout_ms: OK");
        else {
                printf("esp_at_base_set_echo_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_base_get_sysstore_timeout_ms(esp_at_hndl0, &val, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		if (val != 0 && val != 1) {
			printf("esp_at_base_get_sysstore_timeout_ms: FAIL: "
			       "unexpected value %d\n", val);
			errs++;
		}
		else
			puts("esp_at_base_get_sysstore_timeout_ms: OK");
	}
        else {
                printf("esp_at_base_get_sysstore_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_base_set_sysstore_timeout_ms(esp_at_hndl0, 0, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
		puts("esp_at_base_set_sysstore_timeout_ms: OK");
        else {
                printf("esp_at_base_set_sysstore_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_base_get_sysstore_timeout_ms(esp_at_hndl0, &val, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		if (val != 0) {
			printf("esp_at_base_get_sysstore_timeout_ms: FAIL: "
			       "unexpected value %d\n", val);
			errs++;
		}
		else
			puts("esp_at_base_get_sysstore_timeout_ms: OK");
	}
        else {
                printf("esp_at_base_get_sysstore_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_base_deep_sleep_timeout_ms(esp_at_hndl0, 1000, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
		puts("esp_at_deep_sleep_timeout_ms: OK");
        else {
                printf("esp_at_deep_sleep_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        puts("Waiting 2s ...");
        sleep_ms(2000);
        puts("... done.");

        tests++;
        from = get_absolute_time();
        err = esp_at_base_test_timeout_ms(esp_at_hndl0, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_base_test_timeout_ms: OK");
        else {
                printf("esp_at_base_test_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_get_mode_timeout_ms(esp_at_hndl0, &mode, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                if (mode < ESP_AT_WIFI_MODE_STATION
		    || mode >= __ESP_AT_WIFI_MODE_MAX) {
                        printf("esp_at_wifi_get_mode_timeout_ms: FAIL: mode %d",
			       mode);
                        errs++;
                }
                else {
                        puts("esp_at_wifi_get_mode_timeout_ms: OK");
                        printf("\tmode=%d\n", mode);
                }
        }
        else {
                printf("esp_at_wifi_get_mode_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_set_mode_timeout_ms(esp_at_hndl0,
					      ESP_AT_WIFI_MODE_STATION,
					      TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_wifi_set_mode_timeout_ms: OK");
        else {
                printf("esp_at_wifi_set_mode_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_get_dhcp_timeout_ms(esp_at_hndl0, &dhcp, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                puts("esp_at_wifi_get_dhcp_timeout_ms: OK");
                printf("\tsoftap=%d station=%d enable=%d\n", dhcp.softap,
		       dhcp.station, dhcp.enable);
        }
        else {
                printf("esp_at_wifi_get_dhcp_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        dhcp.station = 1;
        dhcp.softap = 0;
        dhcp.enable = 1;
        from = get_absolute_time();
        err = esp_at_wifi_set_dhcp_timeout_ms(esp_at_hndl0, &dhcp, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_wifi_set_dhcp_timeout_ms: OK");
        else {
                printf("esp_at_wifi_set_dhcp_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_get_ap_timeout_ms(esp_at_hndl0, &ap, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                puts("esp_at_wifi_get_ap_timeout_ms: OK");
                printf("\tssid=%s bssid=%s channel=%d rssi=%d pci_en=%d\n",
		       ap.ssid, ap.bssid, ap.channel, ap.rssi, ap.pci_en);
                printf("\treconn=%d listen_interval=%d scan_mode=%d\n",
		       ap.reconn, ap.listen_interval, ap.scan_mode);
        }
        else {
                printf("esp_at_wifi_get_ap_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

	status.remote_ip = remote_ip;

        tests++;
	status.local_port = UINT16_MAX;
        from = get_absolute_time();
        err = esp_at_tcp_get_status_timeout_ms(esp_at_hndl0, &status, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		puts("esp_at_tcp_get_status_timeout_ms: OK");
                printf("\tremote_ip=%s remote_port=%d local_port=%d\n",
		       status.remote_ip, (int)status.remote_port,
		       (int)status.local_port);
		printf("\tstatus=%d type=%d role=%d linkId=%d\n",
		       (int)status.status, (int)status.type, (int)status.role,
		       (int)status.linkId);
        }
        else {
                printf("esp_at_tcp_get_status_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        /* 
	 * AT+CWJAP too rapidly after setting DHCP has caused the
	 * ESP8266 to reset.
	 */
        puts("Sleeping 1s ...");
        sleep_ms(1000);
        puts("... done.");

        /* WiFi creds included from creds.h */
        strcpy(ap.ssid, SSID);
        password = PASSWD;
        ap.bssid[0] = '\0';
        ap.pci_en = -1;

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_set_ap_timeout_ms(esp_at_hndl0, &ap, password,
					    &cx_status, &ip, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                if (!cx_status)
                        puts("esp_at_wifi_set_ap_timeout_ms: FAIL: "
			     "disconnected\n");
                else if (!ip)
                        puts("esp_at_wifi_set_ap_timeout_ms: FAIL: no IP\n");
                else
                        puts("esp_at_wifi_set_ap_timeout_ms: OK");
        }
        else {
                printf("esp_at_wifi_set_ap_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_get_ap_timeout_ms(esp_at_hndl0, &ap, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                if (*ap.ssid == '\0' || *ap.bssid == '\0')
                        printf("esp_at_wifi_get_ap_timeout_ms: FAIL: "
			       "ssid=%s bssid=%s\n", ap.ssid, ap.bssid);
                else {
                        puts("esp_at_wifi_get_ap_timeout_ms: OK");
                        printf("\tssid=%s bssid=%s channel=%d rssi=%d "
			       "pci_en=%d\n", ap.ssid, ap.bssid, ap.channel,
			       ap.rssi, ap.pci_en);
			printf("\treconn=%d listen_interval=%d scan_mode=%d\n",
			       ap.reconn, ap.listen_interval, ap.scan_mode);
                }
        }
        else {
                printf("esp_at_wifi_get_ap_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

	status.remote_port = UINT16_MAX;
	status.local_port = UINT16_MAX;
	status.status = __ESP_AT_STATUS_MAX;
	status.type = __ESP_AT_CX_TYPE_MAX;
	status.role = __ESP_AT_ROLE_MAX;
	status.linkId = INT8_MAX;

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_get_status_timeout_ms(esp_at_hndl0, &status, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		if (status.status != ESP_AT_STATUS_AP) {
			puts("esp_at_tcp_get_status_timeout_ms: FAIL");
			printf("\tExpected status %d, got %d\n",
			       (int)ESP_AT_STATUS_AP, (int)status.status);
			errs++;
		}
		else
			puts("esp_at_tcp_get_status_timeout_ms: OK");
                printf("\tremote_ip=%s remote_port=%d local_port=%d\n",
		       status.remote_ip, (int)status.remote_port,
		       (int)status.local_port);
		printf("\tstatus=%d type=%d role=%d linkId=%d\n",
		       (int)status.status, (int)status.type, (int)status.role,
		       (int)status.linkId);
        }
        else {
                printf("esp_at_tcp_get_status_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_get_recvmode_timeout_ms(esp_at_hndl0, &recv_mode,
						  TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                puts("esp_at_tcp_get_recvmode_timeout_ms: OK");
		printf("\trecv_mode: %d\n", recv_mode);
	}
        else {
                printf("esp_at_tcp_get_recvmode_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_set_recvmode_timeout_ms(esp_at_hndl0,
						  ESP_AT_RECV_MODE_PASSIVE,
						  TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_tcp_set_recvmode_timeout_ms: OK");
        else {
                printf("esp_at_get_recv_mode_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        struct esp_at_cx cx;
        cx.addr = "httpbin.org";
        cx.port = 80;
        cx.type = ESP_AT_CX_TYPE_TCP;
        cx.keepalive = 7200;
        cx.linkId = -1;

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_connect_timeout_ms(esp_at_hndl0, &cx, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_tcp_connect_timeout_ms: OK");
        else {
                printf("esp_at_tcp_connect_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_get_status_timeout_ms(esp_at_hndl0, &status, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		if (status.status != ESP_AT_STATUS_CX) {
			puts("esp_at_tcp_get_status_timeout_ms: FAIL");
			printf("\tExpected status %d, got %d\n",
			       (int)ESP_AT_STATUS_CX, (int)status.status);
			errs++;
		}
		else
			puts("esp_at_tcp_get_status_timeout_ms: OK");
                printf("\tremote_ip=%s remote_port=%d local_port=%d\n",
		       status.remote_ip, (int)status.remote_port,
		       (int)status.local_port);
		printf("\tstatus=%d type=%d role=%d linkId=%d\n",
		       (int)status.status, (int)status.type, (int)status.role,
		       (int)status.linkId);
        }
        else {
                printf("esp_at_tcp_get_status_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        uint8_t rxbuf[RX_BUFLEN], txbuf[] =
		"GET /get HTTP/1.1\r\n"
		"Host: httpbin.org\r\n"
		"Accept: application/json\r\n\r\n";
        
        tests++;
	printf("Sending:\n-----8<-----\n%s-----8<-----\n", txbuf);
        from = get_absolute_time();
        err = esp_at_tcp_send_timeout_ms(esp_at_hndl0, -1, txbuf,
					 sizeof(txbuf)-1, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_tcp_send_timeout_ms: OK");
        else {
                printf("esp_at_tcp_send_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_recv_timeout_ms(esp_at_hndl0, -1, rxbuf, RX_BUFLEN,
					 &resp_len, 3*TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                puts("esp_at_tcp_recv_timeout_ms: OK");
		printf("\tactual_len=%d\n", resp_len);
		puts("\tReceived:\n-----8<-----\n");
		printf("%.*s\n-----8<-----\n", resp_len, rxbuf);
	}
        else {
                printf("esp_at_tcp_recv_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_close_timeout_ms(esp_at_hndl0, -1, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_tcp_close_timeout_ms: OK");
        else {
                printf("esp_at_tcp_close_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_get_status_timeout_ms(esp_at_hndl0, &status, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		if (status.status != ESP_AT_STATUS_NO_CX) {
			puts("esp_at_tcp_get_status_timeout_ms: FAIL");
			printf("\tExpected status %d, got %d\n",
			       (int)ESP_AT_STATUS_NO_CX, (int)status.status);
			errs++;
		}
		else
			puts("esp_at_tcp_get_status_timeout_ms: OK");
                printf("\tremote_ip=%s remote_port=%d local_port=%d\n",
		       status.remote_ip, (int)status.remote_port,
		       (int)status.local_port);
		printf("\tstatus=%d type=%d role=%d linkId=%d\n",
		       (int)status.status, (int)status.type, (int)status.role,
		       (int)status.linkId);
        }
        else {
                printf("esp_at_tcp_get_status_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_disconnect_ap_timeout_ms(esp_at_hndl0, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK)
                puts("esp_at_wifi_disconnect_ap_timeout_ms: OK");
        else {
                printf("esp_at_wifi_disconnect_ap_timeout_ms: FAIL: %s\n",
                    esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_tcp_get_status_timeout_ms(esp_at_hndl0, &status, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
		if (status.status != ESP_AT_STATUS_NO_AP) {
			puts("esp_at_tcp_get_status_timeout_ms: FAIL");
			printf("\tExpected status %d, got %d\n",
			       (int)ESP_AT_STATUS_NO_AP, (int)status.status);
			errs++;
		}
		else
			puts("esp_at_tcp_get_status_timeout_ms: OK");
                printf("\tremote_ip=%s remote_port=%d local_port=%d\n",
		       status.remote_ip, (int)status.remote_port,
		       (int)status.local_port);
		printf("\tstatus=%d type=%d role=%d linkId=%d\n",
		       (int)status.status, (int)status.type, (int)status.role,
		       (int)status.linkId);
        }
        else {
                printf("esp_at_tcp_get_status_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        tests++;
        from = get_absolute_time();
        err = esp_at_wifi_get_ap_timeout_ms(esp_at_hndl0, &ap, TIMEOUT);
        to = get_absolute_time();
        if (err == ESP_AT_ERR_OK) {
                if (ap.ssid[0] != '\0' || ap.bssid[0] != '\0') {
                        printf("esp_at_wifi_get_ap_timeout_ms: FAIL: "
			       "ssid=%s bssid=%s\n", ap.ssid, ap.bssid);
                        errs++;
                }
                else {
                        puts("esp_at_wifi_get_ap_timeout_ms: OK");
			printf("\tssid=%s bssid=%s channel=%d rssi=%d pci_en=%d\n",
			       ap.ssid, ap.bssid, ap.channel, ap.rssi,
			       ap.pci_en);
			printf("\treconn=%d listen_interval=%d scan_mode=%d\n",
			       ap.reconn, ap.listen_interval, ap.scan_mode);
		}
        }
        else {
                printf("esp_at_wifi_get_ap_timeout_ms: FAIL: %s\n",
		       esp_at_errstr[err]);
                errs++;
        }
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

	tests++;
        from = get_absolute_time();
	esp_at_hndl_fini(esp_at_hndl0);
	to = get_absolute_time();
	puts("esp_at_hndl_fini: OK");
        printf("\t%lld us\n\n", absolute_time_diff_us(from, to));

        printf("%d tests\n", tests);
        printf("%d errors\n", errs);
        for (;;)
                __wfi();
}
